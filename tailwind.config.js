module.exports = {
	theme: {

		fontFamily: {
			body: [
				'Inter',
				'-apple-system',
				'BlinkMacSystemFont',
				'"Segoe UI"',
				'"Helvetica Neue"',
				'Arial',
				'sans-serif'
			]
		},

		extend: {

		}
	},

	plugins: [
        // require('@tailwindcss/ui')
    ]
}
