require('dotenv').config();

export default {
	mode: 'spa',

	head: {
		title: 'Applicatienaam',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: '' },
			{ hid: 'og:description', property: 'og:description', content: '' },
			// { hid: 'og:image', property: 'og:image', content: '/cover.jpg' }
		],
		link: [
			// { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap' }
		]
	},

	loading: false,

	components: [
		{ path: '~/components/'},
		// { path: '~/components/sections/', prefix: 'section' }
	],

	tailwindcss: {
		cssPath: '@/assets/main.scss'
	},

	// pwa: {
    //     manifest: {
    //         name: 'Application name',
	// 		short_name: 'Application',
	// 		theme_color: '#0652DD'
    //     }
	// },

	publicRuntimeConfig: {
		// variableName: process.env.VARIABLE_NAME
	},

	plugins: [],

	buildModules: [
		'@nuxtjs/tailwindcss'
	],

	modules: [
		'@nuxtjs/axios',
		'@nuxtjs/pwa',
		'@nuxtjs/svg'
	],

	axios: {
		baseURL: process.env.API_URL || 'http://localhost:8000'
	},

	generate: {
		fallback: true
	},

	purgeCSS: {
		whitelistPatterns: [ /-(leave|enter|appear)(|-(to|from|active))$/]
	}
}
